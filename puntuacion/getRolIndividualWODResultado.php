<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idParticipantes = $input['ids_participantes'];
$idWod = $input['id_wod'];
$order = $input['orden'];

if($passkeyrequest == $passkey){

   $sql = "SELECT t_hit_wod_participante.*, t_participantes.nombre FROM t_hit_wod_participante, t_participantes 
WHERE t_hit_wod_participante.t_participantes_idt_participantes in (".$idParticipantes.")
AND t_hit_wod_participante.t_participantes_idt_participantes = t_participantes.idt_participantes
AND c_wods_idc_wods =".$idWod." ORDER BY t_hit_wod_participante.resultado ".$order.", t_hit_wod_participante.t_participantes_idt_participantes ASC";

    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron Wods para el evento"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se encontraron jueces para el evento"}');
            
        }
    }

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>