<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idsEquipos = $input['ids_equipos'];

if($passkeyrequest == $passkey){

   $sql = "SELECT DISTINCT(c_wods_idc_wods), `c_wods`.`nombre`,`c_wods`.`tipo`
        FROM `t_hit_wod_equipo`,  `c_wods` WHERE t_equipos_idt_equipos in (".$idsEquipos.") 
AND `t_hit_wod_equipo`.`c_wods_idc_wods` = `c_wods`.`idc_wods`
ORDER BY c_wods_idc_wods ASC";
    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron Wods para el evento"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se encontraron jueces para el evento"}');
            
        }
    }

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>