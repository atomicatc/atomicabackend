<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idsTeams = $input['ids_equipos'];
$idWod = $input['id_wod'];
$idJuez = $input['id_juez'];

if($passkeyrequest == $passkey){

    $sql = "SELECT t_hit_wod_equipo.*, t_equipos.nombre FROM t_hit_wod_equipo, t_equipos 
WHERE t_equipos_idt_equipos in (".$idsTeams.") 
AND t_hit_wod_equipo.t_equipos_idt_equipos = t_equipos.idt_equipos
AND t_hit_wod_equipo.c_wods_idc_wods = ".$idWod." AND t_hit_wod_equipo.c_jueces_idc_jueces =  ".$idJuez."
ORDER BY  t_hit_wod_equipo.t_equipos_idt_equipos ASC";

    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron Equipos"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se encontraron Equipos"}');
            
        }
    }

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>