<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idsParticipantes = $input['ids_participantes'];

if($passkeyrequest == $passkey){

    $sql = "SELECT DISTINCT(hit) FROM t_hit_wod_participante WHERE t_participantes_idt_participantes in (".$idsParticipantes.") ORDER BY hit ASC";
    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron Hits para el evento"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se encontraron Hits para el evento"}');
            
        }
    }
} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>