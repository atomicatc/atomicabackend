<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$startDate = $input['start_date'];
$endDate = $input['end_date'];


if($passkeyrequest == $passkey){

    $sql = "SELECT t_acceso_diario.*, t_miembros.nombre_completo 
    FROM t_acceso_diario,t_miembros 
    WHERE t_acceso_diario.idt_miembro = t_miembros.idt_miembros 
    AND t_acceso_diario.fecha_registro between '".$startDate."' AND '".$endDate."'
    ORDER by t_acceso_diario.idt_acceso_diario DESC";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row       
        while($row = $result->fetch_assoc()) {
            $rows[]= $row;
        }
        echo json_encode($rows);
    } else {
        die('{"error":"No se encontraron registros"}');
    }
} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>