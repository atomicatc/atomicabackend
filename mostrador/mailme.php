<?php

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$email = $input['email'];
$name = $input['name'];
$message = $input['message'];

if($passkeyrequest == $passkey){

// multiple recipients (note the commas)
$to = "contacto@macna.com.mx ";
//$to .= "nobody@example.com, ";
//$to .= "somebody_else@example.com";

// subject
$subject = "Mensaje MACNA.COM.MX";

// compose message
$message = "
<html>
  <head>
    <title>Mensaje MACNA.COM.MX</title>
  </head>
  <body>
    <h1>Mensaje MACNA.COM:MX</h1>
    <p>Ha recibido un mensaje desde el sitio web.</p>
    <ul>
        <li>Nombre: ".$name."</li>
        <li>Email: ".$email."</li>
        <li>Mensaje: ".$message."</li>
    </ul>
  </body>
</html>
";

// To send HTML mail, the Content-type header must be set
$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-type: text/html; charset=iso-8859-1\r\n";

// send email
mail($to, $subject, $message, $headers);


} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>