<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idMiembro = $input['id_miembro'];
$codigo = $input['codigo'];

if($passkeyrequest == $passkey){


    $stmt = $conn -> prepare("UPDATE `t_miembros`
    SET  `uuid` = ?
    WHERE `idt_miembros` = ?;");
    $stmt -> bind_param("si",$codigo,$idMiembro);

    $result = $stmt -> execute();
    
    if(!$result){
        die('{"error":"No se registrar el resultado., fallo: '.htmlspecialchars($stmt->error).'"}');
    } else {
        die('{"success":"Resultado guardado correctamente."}');
    }
    $stmt ->close();
    $conn->close();

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>