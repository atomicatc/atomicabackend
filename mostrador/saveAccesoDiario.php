<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idMiembro = $input['id_miembro'];
$codigo = $input['codigo_acceso'];
$detalle = $input['detalle'];

if($passkeyrequest == $passkey){


    $stmt = $conn -> prepare("INSERT INTO `t_acceso_diario`
(`idt_miembro`, `codigo_acceso`, `fecha_registro`, `detalle`)
VALUES (?,?,CURRENT_TIMESTAMP,?);");
    $stmt -> bind_param("iss",$idMiembro,$codigo,$detalle);

    $result = $stmt -> execute();
    
    if(!$result){
        die('{"error":"No se Registro el acceso: '.htmlspecialchars($stmt->error).'"}');
    } else {
        die('{"success":"Acceso guardado correctamente."}');
    }
    $stmt ->close();
    $conn->close();

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>