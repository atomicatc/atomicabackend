<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$id = $input['id_evento'];

if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("DELETE FROM `t_eventos` WHERE idt_eventos =?;");
    $stmt -> bind_param("i",$id);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro elminiar el evento, fallo: '.htmlspecialchars($stmt->error).'"}');
    }
    $stmt ->close();
    $conn->close();
die('{"success":"Registro borrado."}');

} else {
     die('{"error":"Passkey no valido"}');
}
?>