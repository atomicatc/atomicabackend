<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$nombre = $input['nombre'];
$idEquipo = $input['id_equipo'];
$tipo = $input['tipo_equipo'];

if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("UPDATE `t_equipos` SET `nombre` = ?, `tipo_equipo` = ? WHERE `idt_equipos` = ?");
    $stmt -> bind_param("sdd",$nombre,$tipo,$idEquipo);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro actualizar al equipo, fallo: '.htmlspecialchars($stmt->error).'"}');
    }else {
        die('{"success":"Actualización exitosa."}');
    }
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>