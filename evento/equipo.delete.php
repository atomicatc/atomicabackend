<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idEquipo = $input['id_equipo'];

if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("DELETE FROM `t_equipos` WHERE `idt_equipos` = ?");
    $stmt -> bind_param("i",$idEquipo);
    $result = $stmt -> execute();

    if($result === false){
        die('{"error":"No se logro eliminar al equipo, fallo: '.htmlspecialchars($stmt->error).'"}');
    }else {
        die('{"success":"Eliminación exitosa."}');
    }
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>