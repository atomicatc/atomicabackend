<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$id = $input['id_participante'];
$nombre = $input['nombre'];
$nivel = $input['nivel'];
$genero = $input['genero'];
$addIndividual = $input['add_individual'];

if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("UPDATE `t_participantes`
SET `nombre` = ?, `nivel` = ?, `genero` = ?, `add_individual` = ?
WHERE `idt_participantes` = ?;");
    $stmt -> bind_param("sdddd",$nombre,$nivel,$genero,$addIndividual,$id);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro actualizar el miembro, fallo: '.htmlspecialchars($stmt->error).'"}');
    } 
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>