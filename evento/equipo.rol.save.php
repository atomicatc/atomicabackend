<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$hit = $input['hit'];
$idWod = $input['id_wod'];
$idEquipo = $input['id_equipo'];
$idJuez = $input['id_juez'];
$etapa = $input['etapa'];

if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("INSERT INTO `t_hit_wod_equipo` ( `hit`, `c_wods_idc_wods`, `t_equipos_idt_equipos`,`c_jueces_idc_jueces`, `etapa`,`is_activo`) VALUES (?,?,?,?,?,1);");
    $stmt -> bind_param("ddddd",$hit,$idWod,$idEquipo,$idJuez,$etapa);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro agregar al Equipo, fallo: '.htmlspecialchars($stmt->error).'"}');
    } else {
        die('{"success":"'.$stmt->insert_id.'"}');
    }
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>