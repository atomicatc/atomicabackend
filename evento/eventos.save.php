<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$nombre = $input['nombre'];
$slogan = $input['slogan'];
$fechaInicia = $input['fecha_inicia'];
$fechaTermina = $input['fecha_termina'];
$numParticipantes = $input['num_participante'];
$numEquipos = $input['num_equipo'];
$partXEquipo = $input['part_x_equipo'];

if($passkeyrequest == $passkey){

$stmt = $conn -> prepare("INSERT INTO `t_eventos` ( `nombre`, `slogan`, `fecha_evento_inicia`, `fecha_evento_termina`,`num_participante`,`num_equipos`,`part_x_equipo`, `fecha_registro`, `estado`) VALUES (?, ?, ?, ?, ?, ?, ?,CURRENT_TIME, 1);");
$stmt -> bind_param("ssddddd",$nombre,$slogan,$fechaInicia,$fechaTermina,$numParticipantes,$numEquipos,$partXEquipo);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro agregar al evento, fallo: '.htmlspecialchars($stmt->error).'"}');
} else {
    die('{"success":"'.$stmt->insert_id.'"}');
}
$stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>