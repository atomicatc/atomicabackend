<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$nombre = $input['nombre'];
$nivel = $input['nivel'];
$genero = $input['genero'];
$idEquipo = $input['id_equipo'];
$idEvento = $input['id_evento'];
$addIndividual = $input['add_individual'];

if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("INSERT INTO `t_participantes` ( `nombre`, `nivel`, `fecha_registro`, `estado`,`t_eventos_idt_eventos`,`t_equipos_idt_equipos`,`genero`,`add_individual`) VALUES (?, ?, CURRENT_DATE, 1,?,?,?,?);");
    $stmt -> bind_param("sddddd",$nombre,$nivel,$idEvento,$idEquipo,$genero,$addIndividual);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro agregar al miembro, fallo: '.htmlspecialchars($stmt->error).'"}');
    } else {
        die('{"success":"'.$stmt->insert_id.'"}');
    }
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>