<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idTeam = $input['id_equipo'];
$idWod = $input['id_wod'];


if($passkeyrequest == $passkey){

   $sql = "SELECT * FROM t_hit_wod_equipo WHERE t_equipos_idt_equipos = ".$idTeam." 
   AND c_wods_idc_wods = ".$idWod." 
   ORDER BY c_wods_idc_wods ASC";

    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron resultados para el equipo"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se encontraron resultados para el equipo"}');
            
        }
    }

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>