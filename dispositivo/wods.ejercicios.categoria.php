<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$wodEjercicioIds = $input['wod_ejercicio_ids'];

if($passkeyrequest == $passkey){

    $sql = "SELECT c_ejercicio_categoria.*, c_categoria.descripcion as categoria_desc, c_wod_ejercicios.nombre as wod_ejercicio, c_wod_ejercicios.repeticiones 
    FROM c_ejercicio_categoria, c_categoria, c_wod_ejercicios WHERE c_ejercicio_categoria.idc_categoria = c_categoria.idc_categoria 
    AND c_ejercicio_categoria.idc_wod_ejercicios = c_wod_ejercicios.idc_wod_ejercicios 
    AND c_ejercicio_categoria.idc_wod_ejercicios IN (".$wodEjercicioIds.") ORDER BY idc_categoria ASC, c_wod_ejercicios.repeticiones DESC";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $rows[]= $row;
        }
        echo json_encode($rows);
    } else {
        die('{"error":"No se encontraron registros"}');
    }
} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>