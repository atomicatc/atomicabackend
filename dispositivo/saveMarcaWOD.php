<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idWod = $input['id_wod'];
$idMiembro = $input['id_miembro'];
$idCategoria = $input['id_categoria'];
$repeticiones = $input['reps'];
$timecap = $input['time_cap'];
$peso = $input['peso'];

if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("INSERT INTO `t_resultados_wod_miembro`
    (`idt_miembro`, `idc_wod_ejercicios`, `idc_categoria`, `repeticiones`, `timecap`, `peso`, `fecha_registro`)
    VALUES (?,?,?,?,?,?,CURRENT_TIMESTAMP);
    ");
    $stmt -> bind_param("ddddds",$idMiembro,$idWod,$idCategoria,$repeticiones,$timecap,$peso);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro registrar la marca, fallo: '.htmlspecialchars($stmt->error).'"}');
    } else {
        die('{"success":"'.$stmt->insert_id.'"}');
    }
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>