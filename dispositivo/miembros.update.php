<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idMiembro = $input['id_miembro'];
$nombreCompleto = $input['nombre_completo'];
$objetivo = $input['objetivo'];
$consideraciones = $input['consideraciones'];
$telefonoPrincipal = $input['telefono_principal'];


if($passkeyrequest == $passkey){
    

$stmt = $conn -> prepare("UPDATE `t_miembros` SET `nombre_completo` = ?, `objetivo` = ?, `consideraciones` = ?, `telefono_principal` = ? WHERE `idt_miembros` = ?");
$stmt -> bind_param("ssssi",$nombreCompleto,$objetivo,$consideraciones,$telefonoPrincipal,$idMiembro);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro actualizar al miembro, fallo: '.htmlspecialchars($stmt->error).'"}');
}
$stmt ->close();

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
die('{"success":"Actualización exitosa."}');
?>