<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idsWODs = $input['ids_wods'];
$categoria = $input['id_categoria'];

if($passkeyrequest == $passkey){

   $sql = "SELECT t_resultados_wod_miembro.*, t_miembros.nombre_completo, c_categoria.descripcion as cat_desc ,c_wods.nombre as wod_desc
   FROM t_resultados_wod_miembro, t_miembros, c_categoria, c_wods
    WHERE t_resultados_wod_miembro.idt_miembro = t_miembros.idt_miembros
    AND t_resultados_wod_miembro.idc_categoria = c_categoria.idc_categoria
    AND t_resultados_wod_miembro.idc_wod_ejercicios = c_wods.idc_wods
   AND t_resultados_wod_miembro.idc_wod_ejercicios IN (".$idsWODs." )
   AND t_resultados_wod_miembro.idc_categoria = ".$categoria."
   ORDER BY t_resultados_wod_miembro.timecap ASC;";

    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron Wods para el dia"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se Marcas para miembro"}');
            
        }
    }

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>