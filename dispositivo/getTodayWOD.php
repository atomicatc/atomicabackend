<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$dia = $input['dia'];
$mes = $input['mes'];
$anio = $input['anio'];

if($passkeyrequest == $passkey){

   $sql = "SELECT c_wod_calendario.*, c_wods.* , c_tipo_wod.descripcion as wod_tipo  
    FROM c_wod_calendario, c_wods,c_tipo_wod
    WHERE c_wod_calendario.idc_wod_ejercicio =  c_wods.idc_wods
    AND c_wods.tipo = c_tipo_wod.idc_tipo_wod
    AND dia=".$dia." AND mes =".$mes." AND anio = ".$anio." 
    ORDER BY idc_wod_calendario DESC LIMIT 1";

    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron Wods para el dia"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se encontraron jueces para el evento"}');
            
        }
    }

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>