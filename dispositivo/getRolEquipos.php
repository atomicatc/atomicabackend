<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idsTeams = $input['ids_equipos'];
$idWod = $input['id_wod'];
$order = $input['orden'];

if($passkeyrequest == $passkey){

   $sql = "SELECT t_hit_wod_equipo.*, t_equipos.nombre as nombre_equipo FROM t_hit_wod_equipo, t_equipos 
            WHERE t_equipos_idt_equipos in (".$idsTeams.") 
            AND t_hit_wod_equipo.t_equipos_idt_equipos = t_equipos.idt_equipos
            AND  c_wods_idc_wods = ".$idWod." ORDER BY t_hit_wod_equipo.resultado ".$order.", t_hit_wod_equipo.t_equipos_idt_equipos= ASC";

    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron Wods para el evento"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se encontraron jueces para el evento"}');
            
        }
    }

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>