<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idMarca = $input['id_marca'];



if($passkeyrequest == $passkey){
    

$stmt = $conn -> prepare("DELETE FROM `t_resultados_wod_miembro`
WHERE idt_resultados_wod_miembro = ?");
$stmt -> bind_param("i",$idMarca);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro eliminar, fallo: '.htmlspecialchars($stmt->error).'"}');
}
$stmt ->close();

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
die('{"success":"Marca eliminada exitosa."}');
?>