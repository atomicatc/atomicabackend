<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$idMiembro = $input['id_miembro'];

if($passkeyrequest == $passkey){

   $sql = "SELECT t_resultados_wod_miembro.*, c_wods.nombre as wod_desc, c_categoria.descripcion as cat_desc
   FROM t_resultados_wod_miembro, c_wods, c_categoria
   WHERE t_resultados_wod_miembro.idc_wod_ejercicios = c_wods.idc_wods
   AND t_resultados_wod_miembro.idc_categoria = c_categoria.idc_categoria
   AND t_resultados_wod_miembro.idt_miembro = ".$idMiembro."
   ORDER BY t_resultados_wod_miembro.timecap ASC";

    $result = $conn->query($sql);
    
    if($result === false){
        die('{"error":"No se encontraron Wods para el dia"}');
    } else {
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $rows[]= $row;
            }
            echo json_encode($rows);
        } else {
            die('{"error":"No se Marcas para miembro"}');
            
        }
    }

} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>