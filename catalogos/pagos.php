<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$id = $input['id_miembro'];

$sql = "SELECT * FROM `t_historial_pagos` where `t_miembros_idt_miembros` = ".$id." order by fecha_registro desc";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $rows[]= $row;
    }
    echo json_encode($rows);
} else {
    die('{"error":"No se encontraron registros"}');
}
$conn->close();
?>