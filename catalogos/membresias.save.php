<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$nombreMebresia = $input['nombreMembresia'];
$costoMembresia = $input['costoMembresia'];
$idPeriodo = $input['idc_periodo'];
$idDiasAsistencia = $input['idc_dias_asistencia'];


$stmt = $conn -> prepare("INSERT INTO `c_membresias` (NOMBRE, COSTO, FECHA_REGISTRO, ESTADO, C_DIAS_ASISTEN_IDC_DIAS_ASISTENCIA, C_PERIODOS_IDC_PERIODOS) VALUES (?,?,CURRENT_TIME,1,?,?)");
$stmt -> bind_param("siii",$nombreMebresia,$costoMembresia,$idDiasAsistencia,$idPeriodo);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro elminiar la membresia, fallo: '.htmlspecialchars($stmt->error).'"}');
}
$stmt ->close();
$conn->close();
die('{"success":"Registro exitoso."}');
?>