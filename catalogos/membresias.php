<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT `c_membresias`.`idc_membresias`,`c_membresias`.`nombre`,`c_membresias`.`costo`,`c_membresias`.`fecha_registro`,`c_membresias`.`estado`,`c_membresias`.`c_dias_asisten_idc_dias_asistencia`,`c_membresias`.`c_periodos_idc_periodos`,
 `c_dias_asisten`.`descripcion` as `c_dias_descripcion`  ,`c_periodos` .`descripcion` as `c_periodo_descripcion` 
FROM `c_membresias`, `c_periodos`, `c_dias_asisten` 
WHERE `c_membresias`.`c_dias_asisten_idc_dias_asistencia` = `c_dias_asisten`.`idc_dias_asistencia`
AND `c_membresias`.`c_periodos_idc_periodos`= `c_periodos`.`idc_periodos`
AND `c_membresias`.`estado` = 1";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $rows[]= $row;
    }
    echo json_encode($rows);
} else {
    die('{"error":"No se encontraron registros"}');
}
$conn->close();
?>