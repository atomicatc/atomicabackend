<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$wodId = $input['wod_id'];

if($passkeyrequest == $passkey){

    $sql = "SELECT * FROM c_wod_ejercicios WHERE c_wods_idc_wods =".$wodId." ORDER by repeticiones DESC";

    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $rows[]= $row;
        }
        echo json_encode($rows);
    } else {
        die('{"error":"No se encontraron registros"}');
    }
} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>