<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$idPago = $input['id_pago'];

$stmt = $conn -> prepare("UPDATE `t_historial_pagos` SET estado = 0 where idt_historial_pagos = ?");
$stmt -> bind_param("i",$idPago);

$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro cancelar el pago, fallo: '.htmlspecialchars($stmt->error).'"}');
}
$stmt ->close();
$conn->close();
die('{"success":"Cancelacion de pago exitosa."}');
?>