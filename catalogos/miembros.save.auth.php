<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$nombre = $input['nombre_completo'];
//$idMembresia = $input['id_membresia'];
//$fechaIngreso = $input['fecha_ingreso'];
//$numeroTelefonico = $input['numero_telefonico'];
$correoElectronico = $input['correo_electronico'];
//$uuid = $input['uuid'];
if($passkeyrequest == $passkey){
   

$stmt = $conn -> prepare("INSERT INTO `t_miembros` 
    (nombre_completo , fecha_ingreso, telefono_principal, email, fecha_registro, estado, c_membresias_idc_membresias,uuid) 
    VALUES (?,0,0,?,CURRENT_DATE,1,1,0);");
$stmt -> bind_param("ss",$nombre,$correoElectronico);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro registrar la cuenta, fallo: '.htmlspecialchars($stmt->error).'"}');
} else {
    die('{"success":"'.$stmt->insert_id.'"}');
}
$stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>