<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$sql = "SELECT t_miembros.*,
    c_membresias.nombre as c_membresia_nombre,
    c_periodos.descripcion as c_periodo_descripcion, c_periodos.medida_tipo as c_periodo_medida_tipo, c_periodos.medida as c_periodo_medida
FROM t_miembros, c_membresias, c_periodos
WHERE t_miembros.c_membresias_idc_membresias = c_membresias.idc_membresias
AND c_membresias.c_periodos_idc_periodos = c_periodos.idc_periodos
AND t_miembros.estado = 1
ORDER BY t_miembros.nombre_completo ASC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $rows[]= $row;
    }
    echo json_encode($rows);
} else {
    die('{"error":"No se encontraron registros"}');
}
$conn->close();
?>