<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$idMiembro = $input['id_miembro'];
$nombre = $input['nombre'];
$idMembresia = $input['id_membresia'];
$fechaIngreso = $input['fecha_ingreso'];
$numeroTelefonico = $input['numero_telefonico'];
$correoElectronico = $input['correo_electronico'];
$isNuevo = $input['is_nuevo'];


$stmt = $conn -> prepare("UPDATE `t_miembros` SET `nombre_completo` = ?, `fecha_ingreso` = ?, `telefono_principal` = ?, `email` = ?, `c_membresias_idc_membresias` = ?, `is_nuevo` = ? WHERE `idt_miembros` = ?");
$stmt -> bind_param("ssssiii",$nombre,$fechaIngreso,$numeroTelefonico,$correoElectronico,$idMembresia,$isNuevo,$idMiembro);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro actualizar al miembro, fallo: '.htmlspecialchars($stmt->error).'"}');
}
$stmt ->close();
$conn->close();
die('{"success":"Actualización exitosa."}');
?>