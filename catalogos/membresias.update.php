<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array


$nombreMebresia = $input['nombreMembresia'];
$costoMembresia = $input['costoMembresia'];
$idPeriodo = $input['idc_periodo'];
$idDiasAsistencia = $input['idc_dias_asistencia'];
$id = $input['idMembresia'];

$stmt = $conn -> prepare("UPDATE `c_membresias` SET nombre = ?, costo= ?, c_dias_asisten_idc_dias_asistencia = ?, c_periodos_idc_periodos = ? WHERE IDC_MEMBRESIAS =?;");
$stmt -> bind_param("siiii",$nombreMebresia,$costoMembresia,$idDiasAsistencia,$idPeriodo,$id);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro actualizar la membresia, fallo: '.htmlspecialchars($stmt->error).'"}');
}
$stmt ->close();
$conn->close();
die('{"success":"Registro borrado."}');
?>