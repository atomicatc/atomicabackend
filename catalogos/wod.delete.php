<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$id = $input['wod_id'];

/* TODO: Actualizar el QUery y probar.*/
$stmt = $conn -> prepare("UPDATE c_wods SET estado = 0 WHERE idc_wods = ?;");
$stmt -> bind_param("i",$id);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro elminiar la membresia, fallo: '.htmlspecialchars($stmt->error).'"}');
}
$stmt ->close();
$conn->close();
die('{"success":"Registro borrado."}');
?>