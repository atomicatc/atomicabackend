<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$idMiembro = $input['id_miembro'];
$concepto = $input['concepto'];
$importe = $input['importe'];
$fechaPago = $input['fecha_pago'];



$stmt = $conn -> prepare("INSERT INTO `t_historial_pagos` (`concepto`,`importe`,`fecha_pago`,`fecha_registro`,`estado`,`t_miembros_idt_miembros`) VALUES (?,?,?,CURRENT_TIME,1,?);");
$stmt -> bind_param("sdsi",$concepto,$importe,$fechaPago,$idMiembro);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro elminiar la membresia, fallo: '.htmlspecialchars($stmt->error).'"}');
}
$stmt ->close();
$conn->close();
die('{"success":"Registro exitoso."}');
?>