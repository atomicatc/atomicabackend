<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$wodId = $input['wod_id'];
$descripcion = $input['descripcion'];
$repeticiones = $input['reps'];
$peso = $input['peso'];


if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("INSERT INTO `c_wod_ejercicios`(`c_wods_idc_wods`,`nombre`,`repeticiones`,`peso`)
VALUES (?,?,?,?);");
    $stmt -> bind_param("isis",$wodId,$descripcion,$repeticiones,$peso);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro agregar al wod y ejercicio, fallo: '.htmlspecialchars($stmt->error).'"}');
    } else {
        die('{"success":"'.$stmt->insert_id.'"}');
    }
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>