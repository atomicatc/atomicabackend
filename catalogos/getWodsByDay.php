<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$dia = $input['dia'];
$mes = $input['mes'];
$anio = $input['anio'];


if($passkeyrequest == $passkey){

    $sql = "SELECT c_wods.*, c_tipo_wod.descripcion as tipo_wod FROM c_wods, c_wod_calendario, c_tipo_wod
WHERE c_wods.idc_wods = c_wod_calendario.idc_wod_ejercicio
AND c_wods.tipo = c_tipo_wod.idc_tipo_wod
    AND c_wod_calendario.dia=".$dia." 
    AND c_wod_calendario.mes=".$mes."
    AND c_wod_calendario.anio=".$anio." 
    AND c_wods.estado = 1
    ORDER BY c_wods.idc_wods ASC;";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $rows[]= $row;
        }
        echo json_encode($rows);
    } else {
        die('{"error":"No se encontraron registros"}');
    }
} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>