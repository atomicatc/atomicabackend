<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$iniciaPeriodo = $input['inicia_periodo'];
$terminaPeriodo = $input['termina_periodo'];

if($passkeyrequest == $passkey){

    $sql = "SELECT t_historial_pagos.idt_historial_pagos as pado_id,t_historial_pagos.concepto as pago_concepto, t_historial_pagos.fecha_registro as pago_fecha,t_historial_pagos.estado as pago_estado,  t_historial_pagos.importe as pago_cantidad,
    t_miembros.* FROM t_historial_pagos, t_miembros
WHERE t_historial_pagos.t_miembros_idt_miembros = t_miembros.idt_miembros
AND t_historial_pagos.fecha_pago BETWEEN '".$iniciaPeriodo."' AND '".$terminaPeriodo."'
AND t_historial_pagos.estado = 1 
ORDER BY t_historial_pagos.fecha_registro DESC";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
            $rows[]= $row;
        }
        echo json_encode($rows);
    } else {
        die('{"error":"No se encontraron registros"}');
    }
} else {
    die('{"error":"Passkey no valido"}');
}
$conn->close();
?>