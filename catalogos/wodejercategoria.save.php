<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');

$input = json_decode($inputJSON, TRUE); //convert JSON into array
//$input = json_decode(trim($inputJSON), TRUE);

$passkeyrequest = $input['pass_key'];
$categoriaId = $input['id_categoria'];
$wodEjercicioId = $input['id_wodejercicio'];
$ejer = $input['ejercicio'];


if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("INSERT INTO c_ejercicio_categoria(idc_categoria, idc_wod_ejercicios, ejercicio) VALUES (?,?,?)");
    $stmt -> bind_param("iis",$categoriaId,$wodEjercicioId,$ejer);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro agregar al wod y ejercicio, fallo: '.htmlspecialchars($stmt->error).'"}');
    } else {
        die('{"success":"'.$stmt->insert_id.'"}');
    }
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>