<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$nombre = $input['nombre'];
$idMembresia = $input['id_membresia'];
$fechaIngreso = $input['fecha_ingreso'];
$numeroTelefonico = $input['numero_telefonico'];
$correoElectronico = $input['correo_electronico'];
$isNuevo = $input['is_nuevo'];


$stmt = $conn -> prepare("INSERT INTO `t_miembros` (nombre_completo , fecha_ingreso, telefono_principal, email, fecha_registro, estado, c_membresias_idc_membresias,is_nuevo) VALUES (?,?,?,?,CURRENT_DATE,1,?,?);");
$stmt -> bind_param("ssssii",$nombre,$fechaIngreso,$numeroTelefonico,$correoElectronico,$idMembresia,$isNuevo);
$result = $stmt -> execute();
if($result === false){
    die('{"error":"No se logro agregar al miembro, fallo: '.htmlspecialchars($stmt->error).'"}');
} else {
    die('{"success":"'.$stmt->insert_id.'"}');
}
$stmt ->close();
$conn->close();
die('{"success":"Registro exitoso."}');
?>