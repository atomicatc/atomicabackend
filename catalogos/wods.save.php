<?php
include_once("../db.php");

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//Obtenemos los datos del body del request
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

$passkeyrequest = $input['pass_key'];
$nombre = $input['nombre'];
$tipoId = $input['tipo_wod'];
$timecap = $input['timecap'];
$series = $input['series'];
$participacion = $input['participa'];
$detalle = $input['detalle'];


if($passkeyrequest == $passkey){

    $stmt = $conn -> prepare("INSERT INTO `c_wods`(`nombre`,`tipo`,`fecha_registro`,`estado`,`timecap`,`series`,`participacion`,`detalle`)
VALUES (?,?,CURRENT_TIME,1,?,?,?,?);");
    $stmt -> bind_param("sddsss",$nombre,$tipoId,$timecap,$series,$participacion,$detalle);
    $result = $stmt -> execute();
    if($result === false){
        die('{"error":"No se logro agregar al wod, fallo: '.htmlspecialchars($stmt->error).'"}');
    } else {
        die('{"success":"'.$stmt->insert_id.'"}');
    }
    $stmt ->close();
} else {
     die('{"error":"Passkey no valido"}');
}
$conn->close();
?>